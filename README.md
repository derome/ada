# ADA

## Cours d'analyse des données avancée
### Master Physique 
### Université Grenoble Alpes

Références analyse des données :
- [Cours de G. Cowann, rappels de base de l'analyse des données](https://www.pp.rhul.ac.uk/~cowan/stat/stat_1.pdf)
- G. Cowan, Statistical Data Analysis, Clarendon, Oxford, 1998
- R.J. Barlow, Statistics: A Guide to the Use of Statistical Methods in the Physical Sciences, Wiley, 1989
- Ilya Narsky and Frank C. Porter, Statistical Analysis Techniques in Particle Physics, Wiley, 2014.
- Luca Lista, Statistical Methods for Data Analysis in Particle Physics, Springer, 2017.
- L. Lyons, Statistics for Nuclear and Particle Physics, CUP, 1986
- F. James., Statistical and Computational Methods in Experimental Physics, 2nd ed., World Scientific, 2006
- S. Brandt, Statistical and Computational Methods in Data Analysis, Springer, New York, 1998 (with program library on CD)
- M. Tanabashi et al. (PDG), Phys. Rev. D 98, 030001 (2018); voir aussi [pdg.lbl.gov sections on probability, statistics, Monte Carlo](http://pdg.lbl.gov/2015/reviews/rpp2015-rev-statistics.pdf)

Utiliser le notebook Jupyter :
- Installation sur votre oridnateur grace à [anaconda](https://www.anaconda.com/)
- Utiliser directement le serveur jupyter UGA [https://jupyterhub.u-ga.fr](https://jupyterhub.u-ga.fr) (connexion avec login AGALAN)

Documentation pour l'utilisation Jupyter notebook :
- [Jupyter Shortcuts](https://www.cheatography.com/weidadeyue/cheat-sheets/jupyter-notebook/pdf_bw/)
- [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet)
- [python-numpy tutorial](http://cs231n.github.io/python-numpy-tutorial/)